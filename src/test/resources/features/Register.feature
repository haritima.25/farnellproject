Feature: User Registration
  As a new User I want to be able to register on uk.farnell.com in as few steps as possible.
  I want to be able to choose whether my login details are remembered by the site
  So that I can utilise express login at my next visit.
  I also want to be presented with on screen information that confirms I have successfully registered and see a personalised greeting
  So that I know my details are authenticated and I’m securely logged in.


  Scenario Outline: Verify the user can register and logged in successfully with or without remember me option
    Given the user add registration details with <Remember Me Option>
    When the user clicks register
    Then the user is logged in successfully
    And The user Id <is or not> remembered

    Examples:
      |Remember Me Option| is or not |
      |On                |is         |
      |Off               |not        |
