package com.farnell.helpers.constants;

public class PageConstants {

    //URL
    public static final String HOME_URL = "https://uk.farnell.com";
    public static final String REGISTRATION_PAGE = "UserRegistrationForm";
    public static final String LOGON_PAGE = "LogonForm";

    //UserDetails
    public static final String FIRST_NAME = "TstUsr";
    public static final String LAST_NAME = "FamilyN";
    public static final String GREETING_MESSAGE = "Hi\n";
    public static final String REMEMBER_ME_OPTION_IS = "is";
    public static final String SPECIAL_CHARACTER_AMPERSAND = "@";
    public static final String EMAIL_DOMAIN = "@gmail.com";
    public static final String REMEMBER_ME_OPTION_OFF = "Off";
    public static final String REMEMBER_ME_OPTION_ON = "On";

    public PageConstants() {  }

}
