package com.farnell.helpers.testVariables;

import java.util.HashMap;
import java.util.Map;

public class SessionVariableMap<K> {

    private final Map<K, Object> variableMap = new HashMap<>();

    public <V> void setImmutableSessionVariable(final K key, final V value) {
        if (this.getSessionVariable(key) != null) {
            throw new UnsupportedOperationException(String.format("%s should not be set more than once", key));
        }
        this.setSessionVariable(key, value);
    }

    public <V> void setSessionVariable(final K key, final V value) {
        this.variableMap.put(key, value);
    }

    @SuppressWarnings("unchecked")
    public <T> T getSessionVariable(final K key) {
        return (T) this.variableMap.get(key);
    }

    public void clear() {
        this.variableMap.clear();
    }

}
