package com.farnell.helpers.testVariables;

public final class TestVariables {

    private static final ThreadLocal<SessionVariableMap<TestSessionVariableKeys>> MAP = new ThreadLocal<>();

    private static SessionVariableMap<TestSessionVariableKeys> getMap() {
        if (MAP.get() == null) {
            MAP.set(new SessionVariableMap<>());
        }
        return MAP.get();
    }

    public static String getUsername() {
        return getMap().getSessionVariable(TestSessionVariableKeys.USERNAME);
    }

    public static void setUsername(final String option) {
        getMap().setImmutableSessionVariable(TestSessionVariableKeys.USERNAME, option);
    }
    public static void clearSession() {
        MAP.remove();
    }

    public enum TestSessionVariableKeys {
        USERNAME
    }

}

