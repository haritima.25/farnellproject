package com.farnell.pagesobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.farnell.helpers.constants.PageConstants.LOGON_PAGE;
import static org.junit.Assert.assertTrue;

public class LogInPage extends LoadableComponent<LogInPage> {
    private WebDriver driver;
    private WebDriverWait wait;
    private BasePage basePage;
    private LoadableComponent<HomePage> parent;

    public LogInPage(WebDriver driver, LoadableComponent<HomePage> parent) {
        basePage = new BasePage(driver);
        this.driver = driver;
        this.parent = parent;
        this.wait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    WebElement logonId;

    public String getLogonId(){
        return basePage.getAttributeDetail(logonId, "value");
    }

    @Override
    protected void load() {
        parent.get().goToLogInPage();
    }

    @Override
    protected void isLoaded() throws Error {
        assertTrue(this.driver.getCurrentUrl().contains(LOGON_PAGE));
        assertTrue(logonId.isDisplayed());
    }
}
