package com.farnell.pagesobjects;

import com.farnell.utils.PropertiesManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.farnell.helpers.constants.PageConstants.HOME_URL;
import static org.junit.Assert.assertTrue;

public class HomePage extends LoadableComponent<HomePage> {
    BasePage basePage ;
    private WebDriver driver;
    private WebDriverWait wait;

    public HomePage(WebDriver driver) {
        basePage = new BasePage(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);

    }

    // Elements
    WebElement loggedInPar;

    @FindBy(linkText = "Register")
    WebElement register;

    @FindBy(id = "mktg_Cookie_button")
    WebElement acceptButton;

    @FindBy(linkText = "Log out")
    WebElement logOut;

    @FindBy(linkText = "Log In")
    WebElement logIn;

    @FindBy(linkText = "My Account")
    WebElement myAccount;

    @FindBy(id = "mk-lb-outer-wrap")
    WebElement homePageOuterWrap;

    public HomePage acceptCookie(){
        basePage.clickElement(acceptButton);
        return this;
    }

    public String getLoggedInMessage() {
        return basePage.getElementText(loggedInPar);
    }

    public HomePage clickLogOut() {
        basePage.clickElement(logOut);
        return this;
    }

    public HomePage clickMyAccount() {
        basePage.clickElement(myAccount);
        return this;
    }

    public void waitForHomePage() {
        wait.until(ExpectedConditions.visibilityOf(homePageOuterWrap));
    }

    private HomePage clickLogIn() {
        basePage.clickElement(logIn);
        return this;
    }

    public RegisterPage goToRegisterPage() {
        basePage.clickElement(register);
        return new RegisterPage(driver , this);
    }

    public LogInPage goToLogInPage() {
        if(loggedInPar.isDisplayed()) {
        this.clickMyAccount().clickLogOut();
        }
        else {
            this.clickLogIn();
        }
        return new LogInPage(driver , this);
    }

    @Override
    protected void load() {
       this.driver.get(PropertiesManager.getInstance().getPropertyValue(PropertiesManager.PropertyKey.HOME_URL));
       acceptCookie();
    }

    @Override
    protected void isLoaded() throws Error {
        assertTrue(this.driver.getCurrentUrl().contains(HOME_URL));
    }
}
