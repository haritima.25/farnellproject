package com.farnell.pagesobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.farnell.helpers.constants.PageConstants.REGISTRATION_PAGE;
import static org.junit.Assert.assertTrue;

public class RegisterPage extends LoadableComponent<RegisterPage>{
    private WebDriver driver;
    private WebDriverWait wait;
    BasePage basePage;
    private LoadableComponent<HomePage> parent;

    public RegisterPage(WebDriver driver, LoadableComponent<HomePage> parent) {
        basePage = new BasePage(driver);
        this.driver = driver;
        this.parent = parent;
        this.wait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    // Elements
    WebElement userRegistrationContainer;
    WebElement logonId;
    WebElement logonPassword;
    WebElement firstName;
    WebElement email1;
    WebElement registerValidate;
    WebElement rememberMe;

    @FindBy(xpath = "//span[contains(text(),'Remember me')]")
    WebElement rememberMeCheckbox;

    public RegisterPage enterUserName(String userName) {
        basePage.inputText(logonId, userName);
        return this;
    }

    public RegisterPage enterPassword(String pwd) {
        basePage.inputText(logonPassword, pwd);
        return this;
    }

    public RegisterPage enterFullName(String name) {
        basePage.inputText(firstName, name);
        return this;
    }

    public RegisterPage enterEmail(String emailAddress) {
        basePage.inputText(email1, emailAddress);
        return this;
    }

    public RegisterPage clickRegister() {
        basePage.clickElement(registerValidate);
        return this;
    }


    public RegisterPage clickRememberMe() {
        basePage.clickElement(rememberMeCheckbox);
        return this;
    }

    public boolean isSelected() {
        return basePage.isElementSelected(rememberMe);
    }

    @Override
    protected void load() {
        parent.get().goToRegisterPage();
    }

    @Override
    protected void isLoaded() throws Error {
        assertTrue(this.driver.getCurrentUrl().contains(REGISTRATION_PAGE));
        assertTrue(userRegistrationContainer.isDisplayed());
    }

}
