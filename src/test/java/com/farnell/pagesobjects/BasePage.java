package com.farnell.pagesobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    public final WebDriverWait wait;
    public final WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
    }

    public void clickElement(WebElement element){
        wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }

    public void inputText(WebElement element, String name) {
        wait.until(ExpectedConditions.visibilityOf(element)).sendKeys(name);
    }

    public String getElementText(WebElement element) {
        return wait.until(ExpectedConditions.visibilityOf(element)).getText();
    }

    public boolean isElementSelected(WebElement element) {
        return element.isSelected();
    }

    public String getAttributeDetail(WebElement element, String value) {
        return wait.until(ExpectedConditions.visibilityOf(element)).getAttribute(value);
    }
}
