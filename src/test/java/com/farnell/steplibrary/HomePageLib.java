package com.farnell.steplibrary;

import com.farnell.pagesobjects.HomePage;
import org.openqa.selenium.WebDriver;

public class HomePageLib {
    HomePage homePage;

    public HomePageLib(WebDriver driver) {
        homePage = new HomePage(driver);
    }

    public String getUserDetails() {
        homePage.waitForHomePage();
        return homePage.getLoggedInMessage();
    }

}
