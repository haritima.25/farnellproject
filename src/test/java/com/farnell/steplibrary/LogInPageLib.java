package com.farnell.steplibrary;

import com.farnell.pagesobjects.HomePage;
import com.farnell.pagesobjects.LogInPage;
import org.openqa.selenium.WebDriver;

public class LogInPageLib {
    HomePage homePage;
    LogInPage logInPage;

    public LogInPageLib(WebDriver driver) {
        homePage = new HomePage(driver);
        logInPage = new LogInPage(driver, homePage);
    }

    public String getLogInText(){
        return logInPage.getLogonId();
    }

    public void goToLogOnPage() {
        logInPage.get();
    }
}
