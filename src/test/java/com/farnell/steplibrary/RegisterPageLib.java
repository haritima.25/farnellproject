package com.farnell.steplibrary;

import com.farnell.helpers.constants.PageConstants;
import com.farnell.helpers.testVariables.TestVariables;
import com.farnell.pagesobjects.HomePage;
import com.farnell.pagesobjects.RegisterPage;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;

import static com.farnell.helpers.constants.PageConstants.EMAIL_DOMAIN;
import static com.farnell.helpers.constants.PageConstants.REMEMBER_ME_OPTION_OFF;
import static com.farnell.helpers.constants.PageConstants.REMEMBER_ME_OPTION_ON;
import static com.farnell.helpers.constants.PageConstants.SPECIAL_CHARACTER_AMPERSAND;

public class RegisterPageLib {
    HomePage homePage;
    RegisterPage registerPage;

    public RegisterPageLib(WebDriver driver) {
        homePage = new HomePage(driver);
        registerPage = new RegisterPage(driver , homePage);
    }

    public RegisterPageLib fillRegistrationForm(){
        String username = PageConstants.FIRST_NAME + RandomStringUtils.randomAlphabetic(4) + RandomStringUtils.randomNumeric(2);
        registerPage.enterUserName(username)
            .enterPassword(username + SPECIAL_CHARACTER_AMPERSAND)
            .enterEmail(username + EMAIL_DOMAIN)
            .enterFullName(username + " " + PageConstants.LAST_NAME);
        TestVariables.setUsername(username);
        return this;
    }

    public RegisterPageLib selectRememberMe(String option) {
        if ((registerPage.isSelected() && option.equals(REMEMBER_ME_OPTION_OFF))
            || (!registerPage.isSelected() && option.equals(REMEMBER_ME_OPTION_ON)) )
            registerPage.clickRememberMe();
        return this;
    }

    public RegisterPageLib selectRegister() {
        registerPage.clickRegister();
        return this;
    }

    public RegisterPageLib goToRegisterPage() {
        registerPage.get();
        return this;
    }
}
