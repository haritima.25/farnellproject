package com.farnell.stepdefinition;

import com.farnell.driverSetup.TestContext;
import com.farnell.helpers.constants.PageConstants;
import com.farnell.helpers.testVariables.TestVariables;
import com.farnell.steplibrary.HomePageLib;
import com.farnell.steplibrary.LogInPageLib;
import com.farnell.steplibrary.RegisterPageLib;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RegisterSteps {
    TestContext testContext ;
    WebDriver driver;
    HomePageLib homePageLib;
    RegisterPageLib registerPageLib;
    LogInPageLib logInPageLib;

    @Before
    public void createDriver(){
        testContext = TestContext.getInstance();
        driver = testContext.getWebDriver();
        homePageLib = new HomePageLib(driver);
        registerPageLib = new RegisterPageLib(driver);
        logInPageLib = new LogInPageLib(driver);
    }

    @Given("the user add registration details with (.*)")
    public void theUserAddRegistrationDetailsWithRememberMeOption(String option) {
        registerPageLib.goToRegisterPage().fillRegistrationForm().selectRememberMe(option);
    }

    @When("the user clicks register")
    public void theUserClicksRegister() {
        registerPageLib.selectRegister();
    }

    @Then("the user is logged in successfully")
    public void theUserIsLoggedInSuccessfully() {
       assertEquals(homePageLib.getUserDetails(), PageConstants.GREETING_MESSAGE + TestVariables.getUsername().toUpperCase());
    }

    @Then("The user Id (.*) remembered")
    public void theUserIdIsOrNotRemembered(String isRemembered) {
        logInPageLib.goToLogOnPage();
        if(PageConstants.REMEMBER_ME_OPTION_IS.equalsIgnoreCase(isRemembered))
            assertTrue(logInPageLib.getLogInText().equalsIgnoreCase(TestVariables.getUsername()));
        else assertTrue(logInPageLib.getLogInText().equalsIgnoreCase(StringUtils.EMPTY));
    }

    @After
    public void quiteAll(Scenario scenario) {
        testContext.captureFailedScreenShot(scenario).closeAll();
        TestVariables.clearSession();
    }
}
