package com.farnell.driverSetup;

import com.farnell.utils.PropertiesManager;
import cucumber.api.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.BrowserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public final class TestContext {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestContext.class);
    private static final TestContext TEST_CONTEXT = new TestContext();
    public static final String FILE_PATH = "target/Screenshots/";
    private WebDriver webDriver;
    private DriverSetup driverSetup = new DriverSetup();
    private static final ThreadLocal<WebDriver> DRIVER_THREAD = new ThreadLocal();

   public static synchronized TestContext getInstance() {
        return TEST_CONTEXT;
    }

    private TestContext() { }

    public WebDriver getWebDriver() {
        final String browserName = PropertiesManager.getInstance().getPropertyValue(PropertiesManager.PropertyKey.BROWSER);
        if (DRIVER_THREAD.get() == null) {
            if (BrowserType.EDGE.equalsIgnoreCase(browserName)) {
                webDriver = driverSetup.getEdgeDriver();
            }
            else if (BrowserType.CHROME.equalsIgnoreCase(browserName)) {
                webDriver = driverSetup.getChromeDriver();
            }
            else if (BrowserType.FIREFOX.equalsIgnoreCase(browserName)) {
                webDriver = driverSetup.getFirefoxDriver();
            }
            else {
                try {
                    throw new Exception("Unsupported browser Type");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        DRIVER_THREAD.set(webDriver);
        return DRIVER_THREAD.get();
    }

    public void closeAll() {
        DRIVER_THREAD.get().quit();
        DRIVER_THREAD.remove();
    }

    public TestContext captureFailedScreenShot(Scenario scenario) {
        if (scenario.isFailed()) {
            final File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
            final String path = FILE_PATH + scenario.getName() + ".png";
            try {
                FileUtils.copyFile(scrFile, new File(path));
                LOGGER.info("***Placed screen shot in " + path + " ***");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return this;
    }
}
