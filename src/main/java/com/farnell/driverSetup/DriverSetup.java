package com.farnell.driverSetup;

import com.farnell.utils.PropertiesManager;
import com.microsoft.edge.seleniumtools.EdgeDriver;
import com.microsoft.edge.seleniumtools.EdgeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.UnreachableBrowserException;

public final class DriverSetup {

    private WebDriver driver;
    private static final String DISABLE_AUTOMATION_BLINK = "--disable-blink-features=AutomationControlled";
    private static final String START_FULL_SCREEN = "--start-fullscreen";
    private static final String IGNORE_CRTS_ERROR = "--ignore-certificate-errors";
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36";

    public DriverSetup() {

    }

    public WebDriver getChromeDriver() {
        try {
            setupChromeDriverBinary();
            final ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.setAcceptInsecureCerts(true);
            chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
            chromeOptions.addArguments(
                IGNORE_CRTS_ERROR,
                START_FULL_SCREEN,
                DISABLE_AUTOMATION_BLINK,
                "--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36");
            if (Boolean.parseBoolean(PropertiesManager.getInstance().getPropertyValue(PropertiesManager.PropertyKey.HEADLESS))) {
                chromeOptions.addArguments("--headless");
            }
                if (SystemUtils.IS_OS_LINUX) {
                chromeOptions.addArguments(
                    "--allow-running-insecure-content",
                    "--disable-web-security",
                    "--disable-dev-shm-usage",
                    "--headless");
            }

            return new ChromeDriver(ChromeDriverService.createDefaultService(), chromeOptions);
        } catch (UnreachableBrowserException e) {
            throw new UnreachableBrowserException(e.getMessage());
        }

    }

    public WebDriver getEdgeDriver() {
        final EdgeOptions edgeOptions = new com.microsoft.edge.seleniumtools.EdgeOptions();
        edgeOptions.addArguments(DISABLE_AUTOMATION_BLINK,
            START_FULL_SCREEN,
            IGNORE_CRTS_ERROR,
            "--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36");
        edgeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});

        setupEdgeDriverBinary();
        driver = new EdgeDriver(edgeOptions);
        return driver;
    }

    public WebDriver getFirefoxDriver() {
        final FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        firefoxOptions.addArguments(IGNORE_CRTS_ERROR,
            DISABLE_AUTOMATION_BLINK);
        firefoxOptions.addPreference("dom.webdriver.enabled", false);
        firefoxOptions.addPreference("general.useragent.override", USER_AGENT);

        setupFirefoxDriverBinary();
        driver = new FirefoxDriver(firefoxOptions);
        driver.manage().window().fullscreen();
        return driver;
    }

    private void setupChromeDriverBinary() {
        try {
            if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_WINDOWS || SystemUtils.IS_OS_LINUX) {
                WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
                WebDriverManager.chromedriver().setup();
            } else {
                System.getenv();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupEdgeDriverBinary() {
        try {
            if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_WINDOWS || SystemUtils.IS_OS_LINUX) {
                WebDriverManager.getInstance(DriverManagerType.EDGE).setup();
                WebDriverManager.edgedriver().setup();
            } else {
                System.getenv();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupFirefoxDriverBinary() {
        try {
            if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_WINDOWS || SystemUtils.IS_OS_LINUX) {
                WebDriverManager.getInstance(DriverManagerType.FIREFOX).setup();
                WebDriverManager.firefoxdriver().setup();
            } else {
                System.getenv();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
