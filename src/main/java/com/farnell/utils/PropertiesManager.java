package com.farnell.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Properties;

public final class PropertiesManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesManager.class);
    private static final ThreadLocal<PropertiesManager> PROPS_MANAGER_THREAD = new ThreadLocal();
    private Properties properties = new Properties();
    private static final String BROWSER = "browserType";

    private PropertiesManager() {
        this.readAndStoreProperties();
    }

    public static PropertiesManager getInstance() {
        if (PROPS_MANAGER_THREAD.get() == null) {
            PROPS_MANAGER_THREAD.set(new PropertiesManager());
        }

        return PROPS_MANAGER_THREAD.get();
    }

    private void readAndStoreProperties() {
        final String testEnvironment = System.getProperty(BROWSER);
        this.readAndStoreFromPropertyFile("environmentalProperties/testEnvironment.properties", false);

        if (testEnvironment != null) {
            this.properties.setProperty(BROWSER, testEnvironment);
        }

    }

    private void readAndStoreFromPropertyFile(String fileName, boolean failIfNotFound) {
        this.LOGGER.debug(String.format("Looking for %s file. ", fileName));

        try {
            final BufferedReader br = new BufferedReader(new InputStreamReader((InputStream) Objects.requireNonNull(this.getClass().getClassLoader().getResourceAsStream(fileName))));

            try {
                this.LOGGER.debug(String.format("Storing %s. ", fileName));
                this.properties.load(br);
                this.LOGGER.debug(String.format("Properties updated from %s file. ", fileName));
            } catch (Throwable var7) {
                try {
                    br.close();
                } catch (Throwable var6) {
                    var7.addSuppressed(var6);
                }

                throw var7;
            }

            br.close();
        } catch (IOException var8) {
            this.LOGGER.error(var8.getMessage());
            this.LOGGER.error("Failed to load properties file. ");
        } catch (NullPointerException var9) {
            if (failIfNotFound) {
                this.LOGGER.error(String.format("Cannot find %s file. ", fileName));
            } else {
                this.LOGGER.info(String.format("Cannot find %s file. ", fileName));
            }
        }

    }

    public String getPropertyValue(PropertiesManager.PropertyKey propertyKey) {
        return this.getPropertyValue(propertyKey.getKey(), true);
    }

    public String getPropertyValue(String propertyKey, boolean failIfNotFound) {
        this.LOGGER.debug(String.format("Retrieving %s from properties. ", propertyKey));
        final boolean propertyFound = this.properties.containsKey(propertyKey);
        if (propertyFound) {
            return this.properties.getProperty(propertyKey);
        } else {
            this.LOGGER.error("Property missing from the property file: " + propertyKey, new Object[]{failIfNotFound});
            return null;
        }
    }

    public enum PropertyKey {
        BROWSER("browserType"),
        HEADLESS("headless"),
        HOME_URL("homeUrl");

        private final String key;

        PropertyKey(String key) {
            this.key = key;
        }

        protected String getKey() {
            return this.key;
        }
    }
}
