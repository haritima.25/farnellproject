# FarnellProject
This project is design to run automation test on the site : https://uk.farnell.com/ .
It supports browser : chrome, MicrosoftEdge and Firefox
Default test browser type is set to chrome (configurable through testEnvironment.properties)
# Framework Design 
1. Java 11
2. Maven
3. Selenium 3.141.59

## Plugins
Once you've cloned the repo, you'll want to install the following plugins for IntelliJ:
- Checkstyle - IDEA

## Setup Checkstyle
1. Go to preferences
2. In the search bar type: “Checkstyle”
3. Make sure the Checkstyle version is set to: 8.23
4. In the config section activate the ‘+’ symbol
5. On the dialog box write a description (Can be anything) and choose radio option: ‘Use a local checkstyle file’ 
and select the checkstyle in the root directory Checkstyle.xml
6. With the new checkstyle option make sure the ‘Active’ tick box is checked
7. Activate the ‘Apply’ and then ‘Ok’ buttons

# Setup Maven
To be able to run maven from the command line locally, you need to have it installed (unless you are given a machine that is preinstalled).
Type in `mvn --version` in command line and if it is not found, then you will need to get it setup first as a prerequisite.

# Running Locally
If you plan to run tests individually, you will want to make sure that you have a testEnvironment.properties file, located at src/main/resources/environmentalProperties. 
This allows you to set the browserType(chrome/edge/firefox) and browserMode (headless=true/false) you're expecting. 

# Maven Command & variables
The following command is the most basic command to run. 
`mvn clean install`
This command will inherit CucumberOptions from the `RegressionTests` class and get it's properties from the testEnvironment.properties file.

The following command is an example of how the tests for the edge browser could be run by using a combination of the above command line arguments
`mvn clean install -DbrowserType=MicrosoftEdge`
Or
`mvn clean verify -DbrowserType=MicrosoftEdge`

